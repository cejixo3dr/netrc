package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os/user"
	"path/filepath"

	"github.com/pkg/errors"

	netrcpkg "github.com/jdxcode/netrc"
)

type config struct {
	machine  string
	password bool
	login    bool
}

func main() {
	cfg := config{}
	flag.StringVar(&cfg.machine, "m", "", "select machine")
	flag.BoolVar(&cfg.password, "p", false, "show machine's password")
	flag.BoolVar(&cfg.login, "l", false, "show machine's login")
	flag.Parse()

	if cfg.machine == "" || (!cfg.login && !cfg.password) {
		flag.PrintDefaults()
		return
	}

	err := run(context.Background(), cfg)
	if err != nil {
		log.Fatalf("failed: %s", err)
	}
}

func run(_ context.Context, cfg config) error {
	const netrcFileName = ".netrc"

	u, err := user.Current()
	if err != nil {
		return errors.Wrap(err, "can't determine users home directory")
	}

	netrc, err := netrcpkg.Parse(filepath.Join(u.HomeDir, netrcFileName))
	if err != nil {
		return errors.Wrap(err, "can't parse netrc file")
	}

	m := netrc.Machine(cfg.machine)
	if m == nil {
		return errors.Errorf("%s is not defined in .netrc", cfg.machine)
	}

	var out string

	switch {
	case cfg.login:
		out = m.Get("login")
	case cfg.password:
		out = m.Get("password")
	}

	_, err = fmt.Print(out)
	return err
}

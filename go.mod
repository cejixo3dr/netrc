module gitlab.com/cejixo3dr/netrc

go 1.18

require (
	github.com/jdxcode/netrc v0.0.0-20210204082910-926c7f70242a
	github.com/pkg/errors v0.9.1
)
